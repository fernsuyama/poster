# README #

### DOWNLOAD FROM THE SERVER ###

This command creates the <project_folder>, and download the whole project:

* git clone https://fernsuyama@bitbucket.org/fernsuyama/poster.git

### UPDATE YOUR LOCAL PROJECT ###

Inside your <project_folder>. Enter:
 
* git pull

### COMMIT CHANGES TO THE SERVER ###

Inside your <project_folder>. Enter:
 
* git add --all at the command line to add the files or changes to the repository.

* git commit -m '<commit_description>' (for example, git commit -m '20190402-1421-bugfix')

* git push


### CHANGE VERSION - DOWNGRADE OR UPGRADE ###

Inside your <project_folder>. Enter:

* git checkout '<version_code>'

